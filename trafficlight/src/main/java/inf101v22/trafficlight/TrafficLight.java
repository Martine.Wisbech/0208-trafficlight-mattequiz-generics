package inf101v22.trafficlight;

public class TrafficLight implements ITrafficLight {

    /*
     * An enum will, similarly to a class and an interface, define a type.
     * Here we define a type we call "State." A variable of the type State
     * can only have the values BLANK, STOP, GETREADY, GO, HURRY, (or null).
     */
    static enum State {
        BLANK, STOP, GETREADY, GO, HURRY
    }

    /**
     * We define a new type of Exception, called AlreadyStartedException.
     * It is essentially idential to a normal Exception, except it has a
     * different name. The only purpose is that we might want to catch our 
     * AlreadyStartedExceptions without catching other exceptions.
     */
    static class AlreadyStartedException extends Exception {    }

    // Instance variables
    private State state;

    /**
     * Constructs a new TrafficLight object.
     */
    public TrafficLight() {
        this.state = State.BLANK;
    }

    /**
     * Moves to the next state in the state cycle.
     */
    void goToNextState() {
        if (this.state == State.BLANK) {
            this.state = State.BLANK;
        }
        else if (this.state == State.STOP) {
            this.state = State.GETREADY;
        }
        else if (this.state == State.GETREADY) {
            this.state = State.GO;
        }
        else if (this.state == State.GO) {
            this.state = State.HURRY;
        }
        else if (this.state == State.HURRY) {
            this.state = State.STOP;
        }
    }

    /**
     * Turn the trafficlight on. Initiailly, it is in the STOP state.
     * @throws AlreadyStartedException if the traffic light is already in an active state.
     */
    void turnOn() throws AlreadyStartedException {
        if (this.state != State.BLANK) {
            throw new AlreadyStartedException();
        }
        this.state = State.STOP;
    }


    @Override
    public void goToGreen() {
        while (this.state != State.GO) {
            this.goToNextState();
        }
        
    }

    @Override
    public void goToRed() {
        while (this.state != State.STOP) {
            this.goToNextState();
        }
    }

    /*
     * Instead of having separate variables, we have methods that
     * report which lights are currently on. Generally, this is a
     * good practice: the outside should only access information
     * through get-methods, never through directly accessing
     * variables.
     */
    @Override
    public boolean getGreen() {
        return this.state == State.GO;
    }

    @Override
    public boolean getYellow() {
        return this.state == State.GETREADY || this.state == State.HURRY;
    }

    @Override
    public boolean getRed() {
        return this.state == State.STOP || this.state == State.GETREADY;
    }

    @Override
    public String toString() {
        return "TrafficLight[ " + this.state + " ]";
    }
    
}
