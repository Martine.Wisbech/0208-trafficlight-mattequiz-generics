package inf101v22.eksempel;

/**
 * Class that illustrates how to create objects that implements the Runnable interface.
 * Once the Runnable objects are created, we use {@link Timer#timeit(Runnable) Timer.timeit}
 * to measure how long it takes to run.
 */
public class UseTimerFirstSample {
    public static void main(String[] args) {
        int n = 100_000_000;
        long ms;
        Runnable myMethod;

        // Basic use -- this is illustrates the principle. The class RunWastefulTask is below.
        myMethod = new RunWastefulTask(n);
        ms = Timer.timeIt(myMethod);
        System.out.println("Running the wastefull task took " + ms + "ms");
        

        // Advanced use 1
        // We create an object of type Runnable whose class is "anonymous".
        // This saves us from writing an entire class and passing arguments
        // through the constructor, as we did above.
        myMethod = new Runnable() {
            @Override
            public void run() { 
                taskThatTakesTime(n);
            }
        };
        ms = Timer.timeIt(myMethod);
        System.out.println("Running the wastefull task took " + ms + "ms");


        // Advanced use 2
        // This is a shortcut for the advanced use 1
        myMethod = () -> taskThatTakesTime(n);
        ms = Timer.timeIt(myMethod);
        System.out.println("Running the wastefull task took " + ms + "ms");
    }

    /*
     * A class within a class like this behaves exatly like any other
     * class AS LONG AS IT IS STATIC.
     */
    static class RunWastefulTask implements Runnable {
        int howMuchTimeToWaste;

        RunWastefulTask(int howMuchTimeToWaste) {
            this.howMuchTimeToWaste = howMuchTimeToWaste;
        }

        @Override
        public void run() {
            UseTimerFirstSample.taskThatTakesTime(this.howMuchTimeToWaste);
        }
    }

    /**
     * A simple task that requires a bit of time
     * @param howMuchTimeToWaste a positive integer
     * @return returns the sum of integers from 0 to howMuchTimeToWaste
     */
    private static long taskThatTakesTime(long howMuchTimeToWaste) {
        long sum = 0;
        for (int i = 0; i < howMuchTimeToWaste; i++) {
            sum += i;
        }
        return sum;
    }
    

}
