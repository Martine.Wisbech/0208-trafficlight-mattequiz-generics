package inf101v22.eksempel;

public class App implements Runnable {

	static String DEFAULT_TEXT = "Hello World";
	String text;

	public static void main(String[] args) {

        String text = DEFAULT_TEXT;
        if (args.length > 0) {
            text = args[0];
        }
        App app = new App(text);
        app.run();
    }

    App(String text) {
        this.text = text;
    }

    @Override
    public void run() {
        System.out.println(this.text);
    }

}
