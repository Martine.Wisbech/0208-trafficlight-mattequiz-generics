package inf101v22.generics;

public class House implements Comparable<House> {
    
    Integer value;

    House(Integer value) {
        this.value = value;
    }

    @Override
    public int compareTo(House that) {
        if (this.value < that.value) return -1;
        if (this.value > that.value) return 1;
        return 0;
    }


}
