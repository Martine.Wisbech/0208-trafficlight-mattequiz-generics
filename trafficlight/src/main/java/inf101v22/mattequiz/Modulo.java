package inf101v22.mattequiz;

public class Modulo implements Operator {

	@Override
	public char getOperator() {
		return '%';
	}

	@Override
	public int doOperaion(int num1, int num2) {
		return num1 % num2;
	}

}
