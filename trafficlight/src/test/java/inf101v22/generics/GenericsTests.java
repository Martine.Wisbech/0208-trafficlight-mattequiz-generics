package inf101v22.generics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;


public class GenericsTests {
    
    @Test
    void testBox() {
        Box<String> myBox = new Box<String>();
        myBox.put("Hello");
        String s = myBox.pick();
        assertEquals("Hello", s);
    }

    @Test
    void testPair() {
        Pair<String, Integer> myPair = new Pair<>("Torstein", 33);

        assertEquals("Torstein", myPair.first);
        assertEquals(33, myPair.second);
    }

    @Test
    void testfindMostValuable() {
        List<Integer> myList = Arrays.asList(2, 6, 3);
        Integer largest = GenLib.findLargest(myList);
        assertEquals(6, largest);
    }
}
